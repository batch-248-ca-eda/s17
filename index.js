console.log('Hello Wolrd!')


/*
  1. Create a function which is able to prompt the user to provide their fullname, age, and location.
    -use prompt() and store the returned value into a function scoped variable within the function.
    -display the user's inputs in messages in the console.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
*/
  
  //first function here:

 function persoInfo(){
  let name = prompt("What is your name?");
  let age = prompt("How old are you?");
  let loc = prompt("Where do you live?");
  alert("Thank you for your input!");
  console.log("Hello, " + name);
  console.log("You are " + age + " years old.");
  console.log("You live in " + loc);
}

persoInfo();




/*
  2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/

  //second function here:

  function favMusicBands(){
  console.log("1. APO Hiking Society");
  console.log("2. Jay Z");
  console.log("3. Beyonce");
  console.log("4. Ben and Ben");
  console.log("5. Bullet Dumas");
}

favMusicBands();

/*
  3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
    -Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/
  
  //third function here:

  function favMovies(){
  console.log("1. Godland");
  console.log("Rotten Tomatoes Rating: 88%");
  console.log("2. Full Time");
  console.log("Rotten Tomatoes Rating: 95%");
  console.log("3. Ocean Boy");
  console.log("Rotten Tomatoes Rating: 78%");
  console.log("4. Let it Be Morning");
  console.log("Rotten Tomatoes Rating: 100%");
  console.log("5. A Lot of Nothing");
  console.log("Rotten Tomatoes Rating: 46%");
}

favMovies();



/*
  4. Debugging Practice - Debug the following codes and functions to avoid errors.
    -check the variable names
    -check the variable scope
    -check function invocation/declaration
    -comment out unusable codes.
*/


// printUsers();
// let printFriends() = 
function printFriends(){
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:"); 
  let friend2 = prompt("Enter your second friend's name:"); 
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1); 
  console.log(friend2); 
  console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

printFriends();

